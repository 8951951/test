from flask import Flask, jsonify
import os

app = Flask(__name__)

@app.route('/')
def home():
    return 'welcome akshat!'

@app.route('/dashboard')
def dashboard():
    api_token = os.getenv('API_TOKEN', 'MySecureToken')
    api_url = os.getenv('API_URL', 'https://fakeweatherservice.com/getforecast')
    return jsonify({
        'API_TOKEN': api_token,
        'API_URL': api_url
    })

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

