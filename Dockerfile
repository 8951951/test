# Use the official Python image
FROM python:3.12-slim

# Set the working directory
WORKDIR /app

# Copy the requirements file and install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code
COPY . .

# Expose port 5000
EXPOSE 5000

# Set environment variables
ENV API_TOKEN=$API_TOKEN
ENV API_URL=$API_URL

# Command to run the application
CMD ["python", "app.py"]

